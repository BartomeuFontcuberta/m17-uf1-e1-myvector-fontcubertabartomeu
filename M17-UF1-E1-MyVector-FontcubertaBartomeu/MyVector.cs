﻿using System;

namespace M17_UF1_E1_MyVector_FontcubertaBartomeu
{
    class MyVector
    {
        private int[] inici = new int[2];
        private int[] fi = new int[2];
        public MyVector(int xInici, int yInici, int xFi, int yFi)
        {
            inici[0] = xInici;
            inici[1] = yInici;
            fi[0] = xFi;
            fi[1] = yFi;
        }
        public MyVector(int[] inici, int[] fi)
        {
            this.inici = inici;
            this.fi = fi;
        }
        public void setXInici(int coordenada)
        {
            inici[0] = coordenada;
        }
        public void setYInici(int coordenada)
        {
            inici[1] = coordenada;
        }
        public void setXFi(int coordenada)
        {
            fi[0] = coordenada;
        }
        public void setYFi(int coordenada)
        {
            fi[1] = coordenada;
        }
        public void setInici(int[] coordenada)
        {
            inici = coordenada;
        }
        public void setFi(int[] coordenada)
        {
            fi = coordenada;
        }
        public int getXInici()
        {
            return inici[0];
        }
        public int getYInici()
        {
            return inici[1];
        }
        public int getXFi()
        {
            return fi[0];
        }
        public int getYFi()
        {
            return fi[1];
        }
        public int[] getInici()
        {
            return inici;
        }
        public int[] getFi()
        {
            return fi;
        }
        public float getDistancia()
        {
            return (float)Math.Sqrt(Math.Pow(fi[0] - inici[0], 2) + Math.Pow(fi[1] - inici[1], 2));
        }
        public void canviarDireccio()
        {
            int[] temp = inici;
            inici = fi;
            fi = temp;
        } 
        public String toString()
        {
            String frase = ("["+inici[0]+", "+inici[1]+"]"+" -> " + "[" + fi[0] + ", " + fi[1] + "]");
            return frase;
        }
    }
}
