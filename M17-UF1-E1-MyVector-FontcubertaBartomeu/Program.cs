﻿using System;

namespace M17_UF1_E1_MyVector_FontcubertaBartomeu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriu el teu nom:");
            String nom = Console.ReadLine();
            Console.WriteLine("Hola "+nom);
            VectorGame generadorVectors= new VectorGame();
            //Crear vectors
            MyVector[] vectors=generadorVectors.randomVectors(10);
            Console.WriteLine("Vectors no ordenats");
            //Vectors no ordenats
            foreach (MyVector vector in vectors)
            {
                Console.WriteLine(vector.toString());
            }
            //Ordenam vectors per mínima distància d'inici o fi a origen
            Console.WriteLine("Ordenam vectors per mínima distància d'inici o fi a origen");
            MyVector[] vectorsOrdenats = generadorVectors.sortVectors(vectors);
            foreach (MyVector vector in vectorsOrdenats)
            {
                Console.WriteLine(vector.toString());
            }
            //Mostrar direcció vectors ordenats
            Console.WriteLine("Mostrar direcció vectors ordenats");
            generadorVectors.mostrarDireccio(vectorsOrdenats);
            //Vector [0, 0] -> [3, 4]
            Console.WriteLine("Vector [0, 0] -> [3, 4]");
            MyVector vectorProva = new MyVector(0, 0, 3, 4);
            Console.WriteLine("Distància: " + vectorProva.getDistancia());
            Console.WriteLine(vectorProva.toString());
            Console.WriteLine("Canvi direcció");
            vectorProva.canviarDireccio();
            Console.WriteLine(vectorProva.toString());
        }
    }
}
