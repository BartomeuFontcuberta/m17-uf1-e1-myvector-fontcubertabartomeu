﻿using System;

namespace M17_UF1_E1_MyVector_FontcubertaBartomeu
{
    class VectorGame
    {
        public MyVector[] randomVectors(int tamany)
        {
            MyVector[] vectors = new MyVector[tamany];
            Random r = new Random();
            for (int i=0; i<tamany; i++)
            {
                int xInici = r.Next(-10, 11);
                int xFi = r.Next(-10, 11);
                int yInici = r.Next(-10, 11);
                int yFi = r.Next(-10, 11);
                while (xInici==xFi&&yInici==yFi)
                {
                    xInici = r.Next(-10, 11);
                    xFi = r.Next(-10, 11);
                    yInici = r.Next(-10, 11);
                    yFi = r.Next(-10, 11);
                }
                MyVector vector = new MyVector(xInici,yInici,xFi,yFi);
                vectors[i] = vector;
            } 
            return vectors;
        }
        public MyVector[] sortVectors(MyVector[] vectors)
        {
            MyVector[] vectorsOrdenats= new MyVector[vectors.Length];
            float[] distancies = new float[vectors.Length];
            //trobar distancies a origen
            for (int i = 0; i < vectors.Length; i++)
            {
                MyVector vector = vectors[i];
                int[] puntOrigen = { 0, 0 };
                int[] puntInici =vector.getInici();
                int[] puntFi = vector.getFi();
                MyVector iniciAOrigen=new MyVector(puntOrigen,puntInici);
                MyVector fiAOrigen = new MyVector(puntOrigen, puntFi);
                float distanciaAInici = iniciAOrigen.getDistancia();
                float distanciaAFi = fiAOrigen.getDistancia();
                if (distanciaAFi > distanciaAInici)
                {
                    distancies[i] = distanciaAInici;
                }
                else
                {
                    distancies[i] = distanciaAFi;
                }
            }
            int[] posicions = new int[vectors.Length];
            //inicialitzar posicions
            for (int i=0; i < vectors.Length; i++)
            {
                posicions[i] = i;
            }
            //ordenar
            for (int i = 0; i < vectors.Length-1; i++)
            {
                for (int j = i+1; j < vectors.Length; j++)
                {
                    if (distancies[i] > distancies[j])
                    {

                        float canviDis = distancies[i];
                        distancies[i] = distancies[j];
                        distancies[j] = canviDis;
                        int canviPos = posicions[i];
                        posicions[i] = posicions[j];
                        posicions[j] = canviPos;
                    }
                }
            }
            //Colocam al vector colocat
            for (int i = 0; i < vectors.Length; i++)
            {
                vectorsOrdenats[i] = vectors[posicions[i]];
            }
            return vectorsOrdenats;
        }
        public void mostrarDireccio(MyVector[] vectors)
        {
            for (int i = 0; i < vectors.Length; i++)
            {
                MyVector vector = vectors[i];
                int amunt = vector.getYInici() - vector.getYFi();
                int dreta = vector.getXInici()-vector.getXFi();
                Console.Write(vector.toString()+"  ");
                if (amunt < 0)
                {
                    Console.Write("^");
                }
                else if (amunt > 0)
                {
                    Console.Write("v");
                }
                if (dreta < 0)
                {
                    Console.Write(">");
                }
                else if (dreta > 0)
                {
                    Console.Write("<");
                }
                Console.WriteLine();
            }
        }
    }
}
